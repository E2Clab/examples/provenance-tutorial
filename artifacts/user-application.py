import time, random, os
from provlight.workflow import Workflow
from provlight.task import Task
from provlight.data import Data

client_id = os.environ.get('PROVLIGHT_SERVER_TOPIC', "")

def model_training(training_time):
    time.sleep(training_time)
    accuracy = round(random.uniform(0, 1), 2)
    return training_time, accuracy


if __name__ == "__main__":
    # IDs defined in the dataflow specification
    dataflow_id = "model_training"
    transformation_id = "training"
    training_input = "training_input"
    training_output = "training_output"

    wf = Workflow(dataflow_id)
    wf.begin()

    # training 10x with different hyperparameters
    for training_id in range(1, 10):
        # model hyperparameters
        kernel_size = random.randint(1, 10)
        num_kernels = random.randint(8, 16)
        length_of_strides = random.randint(1, 5)
        pooling_size = random.randint(8, 16)
        # training input: model hyperparameters
        model_hyperparameters = {'model_hyperparameters': [
            kernel_size,
            num_kernels,
            length_of_strides,
            pooling_size,
        ]}
        task = Task(int(str(client_id)+str(training_id)), wf, transformation_id, dependencies=[])
        data_in = Data(training_input, dataflow_id, [], model_hyperparameters)
        task.begin([data_in])
        # START training... time to train the model with hyperparameter set
        _training_time, _accuracy = model_training(training_time=random.randint(1, 5))
        # training output: model performance
        data_out = Data(training_output, dataflow_id, [], {'model_performance': [
            _accuracy,
            _training_time,
        ]})
        task.end([data_out])

    wf.end()
